
#include <Common/CSharedCreatureData.h>
#include <Common/Utils.h>

CompileTimeOffsetCheck(CSharedCreatureData, index, 0x0028);
CompileTimeOffsetCheck(CSharedCreatureData, storeMode, 0x06AC);
CompileTimeOffsetCheck(CSharedCreatureData, level, 0x07A4);

